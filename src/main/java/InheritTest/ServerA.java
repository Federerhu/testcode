package InheritTest;

// 抽象類別是個未定義完全的類別，所以它不能被用來生成物件，它只能被擴充，並於擴充後完成未完成的抽象方法定義。
// Interface只能繼承(extends)Interface；Abstract Class能繼承類別也能實作Interface。
// 類別能實作多個Interface，Interface能繼承多個Interface；類別只能繼承一個Abstract Class。
// Interface不可有實作方法（除了static method及Java 8的default method。
// Abstract Class可以有實作方法，也可以有無實作的抽象方法。

// a f aServer
public abstract class ServerA<T extends BJson> {

    // Interface中的變數預設（也只能是）pulbic final static；Abstract Class可自訂變數的存取範圍。
    protected Pools<T> pools;

    // Interface的方法預設（也只能是）public；Abstract Class則可自訂方法的存取範圍（但抽像方法不可為private）。
    public void init(String applicationContext, Class<T> cjClass) throws IllegalAccessException, InstantiationException {
        System.out.println("abstract class ServerA init");
        this.pools = new Pools<>();
        T jc = cjClass.newInstance();
        jc.defaultConfig();
        this.pools.addConfig(jc);
    }


}

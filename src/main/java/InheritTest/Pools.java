package InheritTest;

// ConfigPools
public class Pools<T extends BJson> {

    protected T newConfig;

    Pools() {
        System.out.println("Pools constructor");
    }

    public void addConfig(T config) {
        this.newConfig = config;
        System.out.println(config.getClass().getSimpleName());
    }
}

package StaticTestla;

public class Circle {
    private static float PI = 0.0f;
    private float radius = 0.0f;

    static { //類別的初始化
        System.out.println("靜態建構函式-啟動");
        PI = 3.141592653f;
    }

    {  //類別物件的建立
        System.out.println("instance class");
    }

    public Circle() {  //建構子
        System.out.println("into constructor");
    }

    public Circle(float radius) {
        //this();
        //super();
        System.out.println("建構函式-啟動");
        this.radius = radius;
    }


    public float getArea() {
        return PI * this.radius * this.radius;
    }
}

class Program {

    public static void main(String[] args) {
        Circle circle1 = new Circle(3.0f);
        Circle circle2 = new Circle(5.0f);

        System.out.println(circle1.getArea());
        System.out.println(circle2.getArea());
    }

}

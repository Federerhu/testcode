package ThreadTest;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduledTest {

    private ScheduledExecutorService msgService = Executors.newScheduledThreadPool(2);

    public static void main(String[] args) throws InterruptedException {
        ScheduledTest a = new ScheduledTest();
        //執行完睡五秒在執行下一個
        a.exec();
        Thread.sleep(5000);
        a.execSchedule();
    }

    private void exec() {
        for (int i = 0; i < 10; i++) {
            msgService.execute(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+" :first");
            });
        }
    }

    private void execSchedule() {
        msgService.scheduleAtFixedRate(() -> {
            System.out.println(Thread.currentThread().getName()+" 2nd");
        }, 5, 5, TimeUnit.MILLISECONDS);
    }

}

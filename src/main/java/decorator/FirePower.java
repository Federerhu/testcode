package decorator;

/**
 * @author federer
 */
public class FirePower extends WeaponDecorator {

    public FirePower(Weapon weapon) {
        super("火", 5, weapon);
        System.out.println("FirePower(weapon) this = "+this);
    }
//    FirePower(){};

    @Override
    protected void special() {
        super.special();
        System.out.println("特殊效果:火焰造成 5點傷害");
    }

}

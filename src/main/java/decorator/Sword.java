package decorator;

/**
 * @author federer
 */
public class Sword implements Weapon {

    private String name = "劍";
    private int hitPoint = 10;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getHitPoint() {
        return hitPoint;
    }

    @Override
    public void attack() {
        System.out.println(name + "造成" + hitPoint + "點傷害");
    }

}

package decorator;

/**
 * @author federer
 */
public class Warrior {

    String name;
    Weapon weapon;

    public Warrior(String name, Weapon weapon) {
        this.name = name;
        this.weapon = weapon;
    }

    public void attack() {
        System.out.print(name + "使用");
        weapon.attack();
        // this.getWeapon()為Warrior的weapon
        // 1. sword，Sword.getName 是 "劍"
        // 2. 也就是fireSword，所以FirePower.getName後是 "火"+"劍"
        System.out.println("目前是使用： "+this.getWeapon().getName());
        System.out.println("=========================================");
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

}

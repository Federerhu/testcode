package decorator;

/**
 * @author federer
 */
public class Test {
    public static void main(String[] args) {

        int a = 5;
        int b ;
        int c ;

        b = c = a;
        System.out.println(b);
        System.out.println(c);

        Weapon sword = new Sword();

        // 在劍上附加火屬性
        Weapon fireSword = new FirePower(sword);

        FirePower f = new FirePower(sword);
        WeaponDecorator w = new FirePower(sword);
        System.out.println(f instanceof Weapon);
        System.out.println(f instanceof WeaponDecorator);
        System.out.println(f instanceof FirePower);
        System.out.println("---------------------------");

        //建立一個戰士並配與普通的劍
        Warrior warrior = new Warrior("あああああ ", sword);

        // 攻擊
        warrior.attack();

        // 換成火劍
        warrior.setWeapon(fireSword);

        warrior.attack();

    }
}

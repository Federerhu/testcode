package decorator;

/**
 * @author federer
 */
public interface Weapon {

    String getName(); // 取得武器名稱
    int getHitPoint(); // 取得攻擊點數
    void attack(); // 攻擊

}
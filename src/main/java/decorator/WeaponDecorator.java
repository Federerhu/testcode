package decorator;

/**
 * @author federer
 */
public abstract class WeaponDecorator implements Weapon {

    protected String name;
    protected int hitPoint;
    protected Weapon weapon;

    public WeaponDecorator(String name, int hitPoint, Weapon weapon) {
        this(name);
        this.name = name;
        this.hitPoint = hitPoint;
        this.weapon = weapon;
        System.out.println("WeaponDecorator(name,hitPoint,weapon) this = "+this);
    }

    WeaponDecorator(String name) {
        this();
        System.out.println("WeaponDecorator(name) this = "+this);
    }

    WeaponDecorator() {
        System.out.println("WeaponDecorator() this = "+this);
    }

    @Override
    public String getName() {
        // "火"+"劍"
        return name + weapon.getName();
    }

    @Override
    public int getHitPoint() {
        // 5+10 = 15
        return hitPoint + weapon.getHitPoint();
    }

    protected void special() {
        if (weapon instanceof WeaponDecorator) {
            WeaponDecorator deco = (WeaponDecorator) weapon;
            deco.special();
        }
    }

    @Override
    public void attack() {
        System.out.println(getName() + "造成" + (hitPoint + weapon.getHitPoint()) + "點傷害");
        this.special();
        // this.getWeapon()為FirePower送的weapon 也就是sword，所以Sword.getName後是 "劍"
        System.out.println("這裡是使用： "+this.getWeapon().getName());
    }

    public Weapon getWeapon() {
        return this.weapon;
    }

}

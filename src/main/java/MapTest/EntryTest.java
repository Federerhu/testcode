package MapTest;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class EntryTest {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("AA", "aa");
        map.put("BB", "bb");
        map.put("CC", "cc");
        map.put("DD", "dd");


        // 返回此映射中包含的映射關係的Set集合
        Set<Map.Entry<String, String>> set = map.entrySet();
        System.out.println("set = "+set);
        for (Map.Entry<String, String> entry : set) {
            //每個Entry就是map中一個key及其它對應的value被重新封裝的對象
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}

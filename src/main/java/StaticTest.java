public class StaticTest {

    private static AAAAXXX aaaaxxx;
    public final static AAAAXXX PROTO_GRPC = new AAAAXXX();

    public static void main(String[] args) {
        System.out.println("aaaaxxx = "+aaaaxxx);                   //null 1                null
        System.out.println(xxx(aaaaxxx));                           //sssss 6               sssss
        System.out.println("aaaaxxx = "+aaaaxxx);                   //AAAAXXX@266474c2 7    AAAAXXX@266474c2
    }

    public static final String xxx(AAAAXXX aaaaxxx) {
        AAAAXXX x = aaaaxxx;
//        x = new AAAAXXX();
        aaaaxxx = new AAAAXXX();
        System.out.println("x! = "+x);                              //AAAAXXX@1d44bcfa 2    null
        System.out.println("aaaaxxx! = "+aaaaxxx);                  //null 3                AAAAXXX@1d44bcfa
        aaaaxxx = x;
        System.out.println("aaaaxxx!! = "+aaaaxxx);                 //AAAAXXX@1d44bcfa 4    null
        System.out.println("aaaaxxx!!! = "+StaticTest.aaaaxxx);     //AAAAXXX@1d44bcfa 5    null
        StaticTest.aaaaxxx =new AAAAXXX();
        return "sssss";
    }

}

class AAAAXXX{
    private final static String WS_URL = String.format("ws://%s:%s/websocket","0:0:0:0",10200);
    public static void main(String[] args) {
        System.out.println(StaticTest.PROTO_GRPC);
        System.out.println(WS_URL);
    }
}

package Netty.NettyUpgradeHttpToWs;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.util.CharsetUtil;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import static io.netty.handler.codec.http.HttpUtil.isKeepAlive;
import static io.netty.handler.codec.http.HttpUtil.setContentLength;


public class WebSocketServerHandler extends SimpleChannelInboundHandler<Object> {

    private static final Logger logger = Logger.getLogger(WebSocketServerHandler.class.getName());

    private WebSocketServerHandshaker handshaker;

    /**
     * 接收客戶端發過來的消息並處理
     * FullHttpRequest ：
     * 官網解釋：Combine the {@link HttpRequest} and {@link FullHttpMessage}, so the request is a <i>complete</i> HTTP
     * request.
     * 這個請求是 代表http請求完成的標記。
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg)
            throws Exception {

        if (msg instanceof FullHttpRequest) {//接收到客戶端的握手請求，開始處理握手
            handleHttpRequest(ctx, (FullHttpRequest) msg);
        } else if (msg instanceof WebSocketFrame) {//接收到客戶端發過來的消息（只過濾文本消息），處理后發給客戶端。
            handleWebSocketFrame(ctx, (WebSocketFrame) msg);
        }
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
    }

    private void handleHttpRequest(ChannelHandlerContext ctx, FullHttpRequest req) throws Exception {
        /**
         * 如果不成功或者消息頭不包含"Upgrade"，說明不是websocket連接，報400異常。
         */
        if (!req.decoderResult().isSuccess() || (!"websocket".equals(req.headers().get("Upgrade")))) {
            sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST));
            return;
        }
        /**
         * WebSocket是一種全新的協議，不屬於http無狀態協議，協議名為"ws"，這意味着一個websocket連接地址會是這樣的寫法：
         ws://127.0.0.1:8080/websocket。ws不是http，所以傳統的web服務器不一定支持，需要服務器與瀏覽器同時支持， WebSocket才能正常運行，目前大部分瀏覽器都支持Websocket。
         WebSocketServerHandshaker 官網的解釋是：服務器端Web套接字打開和關閉握手基類
         WebSocketServerHandshakerFactory 官網的解釋是：自動檢測正在使用的網絡套接字協議的版本，並創建一個新的合適的 WebSocketServerHandshaker。
         */
        WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory("ws://localhost:8080/websocket", null, false);
        handshaker = wsFactory.newHandshaker(req);//創建一個握手協議
        if (handshaker == null) {
            /**
             * Return that we need cannot not support the web socket version
             * 返回不支持websocket 版本
             */
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            handshaker.handshake(ctx.channel(), req);//開始握手
        }
    }

    /**
     * 我們判斷數據類型，只支持文本類型
     *
     * @param ctx
     * @param frame
     */
    private void handleWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame frame) {
        if (frame instanceof CloseWebSocketFrame) {//如是接收到的是關閉websocket，就關閉連接
            handshaker.close(ctx.channel(), (CloseWebSocketFrame) frame.retain());
            return;
        }
        if (frame instanceof PingWebSocketFrame) {//如果信息是2進制數據，就反給它，
            ctx.channel().write(new PongWebSocketFrame(frame.content().retain()));
            return;
        }

        if (!(frame instanceof TextWebSocketFrame)) {//哪果不是文本的數據，就報錯。
            throw new UnsupportedOperationException(String.format("%s frame types not supported", frame.getClass().getName()));
        }

        String request = ((TextWebSocketFrame) frame).text();
        if (logger.isLoggable(Level.FINE)) {
            logger.fine(String.format("%s receive %s", ctx.channel(), request));
        }
        ctx.channel().write(new TextWebSocketFrame(request + ",歡迎使用netty websocket 服務，現在時刻" + new Date().toString()));
    }

    private static void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest req, FullHttpResponse res) {
        if (res.getStatus().code() != 200) {
            ByteBuf buf = Unpooled.copiedBuffer(res.getStatus().toString(), CharsetUtil.UTF_8);
            res.content().writeBytes(buf);
            buf.release();
            setContentLength(res, res.content().readableBytes());

        }
        ChannelFuture f = ctx.channel().writeAndFlush(res);//發送消息
        if (!isKeepAlive(req) || res.getStatus().code() != 200) {//如果斷開連接，或者發送不成功，斷開連接。
            f.addListener(ChannelFutureListener.CLOSE);//關閉連接
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
            throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}

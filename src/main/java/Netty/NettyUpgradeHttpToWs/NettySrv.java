package Netty.NettyUpgradeHttpToWs;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;

public class NettySrv {

    public static void main(String[] args) {
        int port = 9090;

        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {

            ServerBootstrap b = new ServerBootstrap();

            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            //HttpServerCodec的作用是將請求和應答消息編碼或者解碼為HTTP消息;
                            ch.pipeline().addLast("http-codec", new HttpServerCodec());
                            //使用HttpObjectAggregator會把多個消息轉換為一個單一的FullHttpRequest或是FullHttpResponse
                            ch.pipeline().addLast("http-aggregator", new HttpObjectAggregator(65536));
                            //支持處理異步發送大數據文件，但不占用過多的內存，防止發生內存泄漏,這里是向客戶端發送html5文件
                            ch.pipeline().addLast("http-chunked", new ChunkedWriteHandler());
                            //這個是我們自定義的，處理文件服務器邏輯。主要功能還是在這個文件中
                            ch.pipeline().addLast("http-fileServerHandler", new WebSocketServerHandler());
                        }
                    });
            Channel ch = b.bind(port).sync().channel(); //這里寫你本機的IP地址
            System.out.println("web socket server started at port " + port + ".");
            System.out.println("open your browser and navigate to http://localhost:" + port + "/");
            ch.closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

package Netty.NettyEchoTest;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;

public final class EchoServer {

    static final boolean SSL = System.getProperty("ssl") != null;
    static final int PORT = Integer.parseInt(System.getProperty("port", "8007"));

    /**
     * @see io.netty.channel.nio.NioEventLoop
     */
    public static void main(String[] args) throws Exception {
        // Configure SSL.
        final SslContext sslCtx;
        if (SSL) {
            SelfSignedCertificate ssc = new SelfSignedCertificate();
            sslCtx = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build();
        } else {
            sslCtx = null;
        }

        // Configure the server.
        // 這兩個對像是整個 Netty 的核心對象，可以說，整個 Netty 的運作都依賴於他們。
        // bossGroup 用於接受 Tcp 請求，他會將請求交給 workerGroup ，workerGroup 會獲取到真正的連接，然後和連接進行通信，比如讀寫解碼編碼等操作。
        EventLoopGroup bossGroup = new NioEventLoopGroup(8);
        EventLoopGroup workerGroup = new NioEventLoopGroup(16);
        try {
            // 创建了一个 ServerBootstrap 对象，他是一个引导类，用于启动服务器和引导整个程序的初始化。
            ServerBootstrap b = new ServerBootstrap();
            //变量 b 调用了 group 方法将两个 group 放入了自己的字段中，用于后期引导使用
            b.group(bossGroup, workerGroup)
                    //new ReflectiveChannelFactory<C>(channelClass)
                    // 添加了一個 channel，其中參數一個Class對象，引導類將通過這個 Class 對象反射創建 Channel。
                    .channel(NioServerSocketChannel.class)
                    //添加了一些TCP的参数。
                    .option(ChannelOption.SO_BACKLOG, 100)
                    // ServerSocketChannel 专属 添加了一个服务器专属的日志处理器 handler
                    .handler(new LoggingHandler(LogLevel.INFO))
                    //添加一个 SocketChannel（不是 ServerSocketChannel）的 handler。
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception { // SocketChannel 专属
                            ChannelPipeline p = ch.pipeline();
                            if (sslCtx != null) {
                                p.addLast(sslCtx.newHandler(ch.alloc()));
                            }
                            //p.addLast(new LoggingHandler(LogLevel.INFO));
                            p.addLast(new EchoServerHandler());
                        }
                    });
            // Start the server. 绑定端口并阻塞至连接成功
            ChannelFuture f = b.bind(PORT).sync();
            System.out.println("server started!!!");
            // Wait until the server socket is closed. main线程阻塞等待关闭
            f.channel().closeFuture().sync();
            System.out.println("server code here");
        } finally {
            // Shut down all event loops to terminate all threads. finally 块中的代码将在服务器关闭时优雅关闭所有资源。
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}

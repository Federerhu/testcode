package InitTest;

public abstract class AAAServer <T extends BBBJson>{

    protected CCCPool<T> pools;

    protected T jc;

    public void init(Class<T> clz){
        this.pools = new CCCPool<T>();
        try {
            this.jc = clz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
